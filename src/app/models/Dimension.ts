import {Atribute} from './Atribute';

export class Dimension {
  name: string;
  atributes: Atribute[];
  checked: boolean;
  sqlName: string;
  sqlKeyFact: string;
  sqlKey: string;

  constructor() {
    this.checked = false;
  }
}
