import {Measurement} from './Measurement';
import {Dimension} from './Dimension';
import {Table} from './Table';
import {Sort} from '@angular/material/sort';

export class TableDetails {
  dimensions: Dimension[];
  measurements: Measurement[];
  table: Table;
  sort: Sort[];
}
