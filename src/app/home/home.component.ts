import {Component, OnInit, ViewChild} from '@angular/core';
import {ServiceService} from '../service.service';
import {Table} from '../models/Table';
import {TableDetails} from '../models/TableDetails';
import {Measurement} from '../models/Measurement';
import {MatTreeFlatDataSource} from '@angular/material/tree';
import {Dimension} from '../models/Dimension';
import {FlatTreeControl} from '@angular/cdk/tree';
import {Atribute} from '../models/Atribute';
import {Analyze} from '../models/Analyze';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public loading = true;
  public tables: Table[];
  public selectedTable: Table;
  public tableDetails: TableDetails;
  public sql: string;
  public analyse: Analyze;

  constructor(private service: ServiceService) {
  }

  ngOnInit() {
    this.service.getTables().subscribe(tables => {
      this.tables = tables;
      this.loading = false;
    });
  }


  public getDetails() {
    if (this.selectedTable !== undefined) {
      this.service.getTableDetails(this.selectedTable.sifTablica).subscribe(tableDetails => {
        console.log(tableDetails);
        this.tableDetails = tableDetails;

      });
    }
  }

  check(measurment: Measurement) {
    measurment.checked = !measurment.checked;
    this.getSql();
  }

  checkAtribute(atribute: Atribute) {
    atribute.checked = !atribute.checked;
    this.getSql();
  }

  checkDimension(dimension: Dimension) {
    dimension.checked = !dimension.checked;
  }

  getSql() {
    this.service.sql(this.tableDetails).subscribe(sql => {
      this.sql = sql.sql;
    });
  }

  analyze() {
    this.analyse = undefined;
    this.service.analyze(this.tableDetails).subscribe(analyse => {
      this.analyse = analyse;
    });
  }
}
