import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Analyze} from '../models/Analyze';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {TableDetails} from '../models/TableDetails';
import {ServiceService} from '../service.service';

@Component({
  selector: 'app-analyse',
  templateUrl: './analyse.component.html',
  styleUrls: ['./analyse.component.css']
})
export class AnalyseComponent implements OnInit {

  @Input()
  public analyse: Analyze;

  @Input()
  tableDetails: TableDetails;

  public numbers: number[];
  public dataSource: MatTableDataSource<string[]>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private service: ServiceService) {
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.analyse.analyzeData);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    const num = this.analyse.columns.length;
    this.numbers = Array(num).fill(0).map((x, i) => i);
  }

  sortData(sort: Sort) {
    if (this.tableDetails.sort === undefined || this.tableDetails.sort === null) {
      this.tableDetails.sort = [];
    }
    this.tableDetails.sort = this.tableDetails.sort.filter(s => s.active !== sort.active);
    this.tableDetails.sort.push(sort);
    this.service.analyze(this.tableDetails).subscribe(analyse => {
      this.analyse = analyse;
      this.dataSource.data = analyse.analyzeData;
    });
  }

}
