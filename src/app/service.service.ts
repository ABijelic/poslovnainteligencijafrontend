import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';
import {Table} from './models/Table';
import {TableDetails} from './models/TableDetails';
import {Dimension} from './models/Dimension';
import {SQL} from './models/SQL';
import {Analyze} from './models/Analyze';


const api = environment.api;

@Injectable({
  providedIn: 'root'
})

export class ServiceService {

  constructor(private http: HttpClient) {
  }

  private getChecked(details: TableDetails): TableDetails {
    const tableDetails = new TableDetails();
    tableDetails.measurements = details.measurements.filter(measurement => measurement.checked);
    const dimms = [];
    details.dimensions.forEach(dimension => {
      const attributes = dimension.atributes.filter(a => a.checked);
      if (attributes.length !== 0) {
        const d = new Dimension();
        d.atributes = attributes;
        d.name = dimension.name;
        d.sqlKey = dimension.sqlKey;
        d.sqlKeyFact = dimension.sqlKeyFact;
        d.sqlName = dimension.sqlName;
        dimms.push(d);
      }
    });
    tableDetails.dimensions = dimms;
    tableDetails.table = details.table;
    tableDetails.sort = details.sort;
    return tableDetails;
  }


  public getTables(): Observable<Table[]> {
    return this.http.get<Table[]>(api + 'tables');
  }

  public getTableDetails(sifTable: number): Observable<any> {
    return this.http.get<any>(api + 'tableDetails/' + sifTable);
  }

  public sql(tableDetails: TableDetails): Observable<SQL> {
    return this.http.post<SQL>(api + 'sql', this.getChecked(tableDetails));
  }

  public analyze(tableDetails: TableDetails): Observable<Analyze> {
    console.log(tableDetails);
    return this.http.post<Analyze>(api + 'analyze', this.getChecked(tableDetails));
  }
}
